package com.example.demo.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import javax.persistence.CascadeType;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import com.example.demo.valueObjects.CategoriaSerie;
import com.example.demo.valueObjects.VisualizacionFactura;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.annotation.JsonView;


@Entity
@Inheritance
public abstract class Usuario {
	
	public static final double PRECIO_GOLD = 1.5;
	public static final double PRECIO_SILVER = 0.75;
	public static final double PRECIO_ESTANDAR = 0.75;
	
	@Id
	@JsonView({com.example.demo.service.api.Views.DescripcionUsuarios.class})
	String nombre; 

	@JsonView({com.example.demo.service.api.Views.DescripcionUsuarios.class})
	
	String IBAN;
	
	@JsonIgnore //Igual mejor no mandarla
	String password;
	
	@OneToMany(mappedBy = "u", cascade = CascadeType.ALL)
	@JsonView({com.example.demo.service.api.Views.DescripcionUsuarios.class})
	@JsonUnwrapped
	List<Factura> historicoFacturas;
	
	/**@OneToOne(mappedBy = "u", cascade = CascadeType.ALL)
	@JsonView({com.example.demo.service.api.Views.DescripcionUsuarios.class})
	Factura facturaMensual;**/
	
	@JsonView({com.example.demo.service.api.Views.DescripcionUsuarios.class})
	@ManyToMany
	@JsonUnwrapped
	Set<Serie> seriesPendientes;
	
	@JsonView({com.example.demo.service.api.Views.DescripcionUsuarios.class})
	@ManyToMany
	@JsonUnwrapped
	Set<Serie> seriesFinalizadas;
	
 
	@OneToMany(cascade = CascadeType.ALL)
	@JsonView({com.example.demo.service.api.Views.DescripcionUsuarios.class})
	Set<UltimoCapituloSerie> ultimoCapituloSeriesEmpezadas;
	
	public Usuario(){
		this.historicoFacturas = new ArrayList<Factura>();
		
		Factura facturaMensual = new Factura(this);
		facturaMensual.total= 0.0;
		facturaMensual.fecha = new Date();
		
		historicoFacturas.add(facturaMensual);
		
		this.seriesPendientes = new HashSet<Serie>();
		this.seriesFinalizadas = new HashSet<Serie>();
		this.ultimoCapituloSeriesEmpezadas = new HashSet<UltimoCapituloSerie>();
		
	}
	
	public Factura[] aextraerFacturas() {
		return (Factura[]) historicoFacturas.toArray();
	}
	
	
	public abstract Factura comprobarFacturaActual();
	
	@JsonView({com.example.demo.service.api.Views.DescripcionUsuarios.class})
	public abstract String getPlan();
	

	public boolean anhadeSerieAPendientes(Serie s) {
		if(!this.seriesFinalizadas.contains(s) && this.ultimoCapituloSeriesEmpezadas.stream().filter(a-> a.s.equals(s)).count() == 0)
			return seriesPendientes.add(s);
		return false;
	}
	
	public void visualizaCapitulo(Capitulo c) {
		
		if(seriesPendientes.contains(c.s)) {
			seriesPendientes.remove(c.s);
		}
		
		if(seriesFinalizadas.contains(c.s)) {
			return;
		}
		
		Optional<UltimoCapituloSerie> ucsOP = this.ultimoCapituloSeriesEmpezadas.stream().filter(ul -> ul.s.equals(c.s)).findFirst();
		UltimoCapituloSerie actual = null;
		boolean fin = false;
		
		if (ucsOP.isPresent()) {
			
			UltimoCapituloSerie ucs = ucsOP.get();
			actual = ucs;
			fin = ucs.setTemporadaCapitulo(c.t.numero, c.numero);
		}else {
			UltimoCapituloSerie serieNueva = new UltimoCapituloSerie(c.getS());			
			this.ultimoCapituloSeriesEmpezadas.add(serieNueva);
			actual = serieNueva;
			fin = serieNueva.setTemporadaCapitulo(c.t.numero, c.numero);
		}
		
		if(fin) {
			this.ultimoCapituloSeriesEmpezadas.remove(actual);
			this.seriesFinalizadas.add(actual.getS());
		}
		
		facturaCapitulo(c);
	}
	
	public void facturaCapitulo( Capitulo c) {
		double precio;
		
		if(c.t.s.categoria == CategoriaSerie.STANDART)
			precio = PRECIO_ESTANDAR;
		else if (c.t.s.categoria == CategoriaSerie.SILVER)
			precio = PRECIO_SILVER;
		else
			precio = PRECIO_GOLD;
		
		VisualizacionFactura visualizacion = new VisualizacionFactura();
		visualizacion.setSerieId(c.getS().id);
		visualizacion.setNumTemporada(c.t.numero);
		visualizacion.setNumeroCapitulo(c.numero);
		visualizacion.setPrecio(precio);
		visualizacion.setFecha(new Date());
		visualizacion.setSerie(c.getS().nombre);
		
		getFacturaMensual().facturar(visualizacion);
	}
	
	public void cobrarFacturaActual() {
		Factura f = new Factura(this);
		f.total= 0.0;
		f.fecha = new Date();
		this.historicoFacturas.add(f);
	}
	
	public Optional<Factura> getFacturaById(Long id) {
		return this.historicoFacturas.stream().filter(a -> a.id == id).findFirst();
	}
	
	public abstract double getTotalFactura();
	
	/*public Serie[] getListaSeriesTerminadas(){
		
		return (Serie[]) this.seriesFinalizadas.toArray();
		//return (Serie[]) this.ultimoCapituloSeriesEmpezadas.stream().filter(s-> s.estaFinalizada()).toArray();
	}*/
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getIBAN() {
		return IBAN;
	}

	public void setIBAN(String iBAN) {
		IBAN = iBAN;
	}

	public List<Factura> getHistoricoFacturas() {
		return historicoFacturas;
	}

	public void setHistoricoFacturas(List<Factura> historicoFacturas) {
		this.historicoFacturas = historicoFacturas;
	}

	public Factura getFacturaMensual() {
		return this.historicoFacturas.get(this.historicoFacturas.size() -1);
	}

	/*public void setFacturaMensual(Factura facturaMensual) {
		this.facturaMensual = facturaMensual;
	}*/

	public Set<Serie> getListaSeriesPendientes() {
		return seriesPendientes;
	}

	public void setListaSeriesPendientes(Set<Serie> listaSeriesPendientes) {
		this.seriesPendientes = listaSeriesPendientes;
	}

	public Set<UltimoCapituloSerie> getUltimoCapituloSeriesEmpezadas() {
		return ultimoCapituloSeriesEmpezadas;
	}

	public void setUltimoCapituloSeriesEmpezadas(Set<UltimoCapituloSerie> ultimoCapituloSeriesEmpezadas) {
		this.ultimoCapituloSeriesEmpezadas = ultimoCapituloSeriesEmpezadas;
	}

	public static double getPrecioGold() {
		return PRECIO_GOLD;
	}

	public static double getPrecioSilver() {
		return PRECIO_SILVER;
	}

	public static double getPrecioEstandar() {
		return PRECIO_ESTANDAR;
	}
	
	public Set<Serie> getSeriesPendientes() {
		return seriesPendientes;
	}

	public void setSeriesPendientes(Set<Serie> seriesPendientes) {
		this.seriesPendientes = seriesPendientes;
	}
}
