package com.example.demo.entities;

import javax.persistence.Entity;

@Entity
public class UsuarioNormal extends Usuario {
	
	
	public UsuarioNormal() {
		super();
	}

	@Override
	public Factura comprobarFacturaActual() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void visualizaCapitulo(Capitulo c) {
		super.visualizaCapitulo(c);
		// TODO Auto-generated method stub

	}

	@Override
	public void cobrarFacturaActual() {
		this.getFacturaMensual().total =
				super.getFacturaMensual().visualizaciones.stream().mapToDouble(t -> t.getPrecio()).reduce(0.0,Double::sum);
		super.cobrarFacturaActual();

	}

	@Override
	public double getTotalFactura() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getPlan() {
		// TODO Auto-generated method stub
		return "Normal";
	}

}
