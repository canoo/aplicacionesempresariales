package com.example.demo.entities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.example.demo.valueObjects.ActorSerie;
import com.example.demo.valueObjects.CategoriaSerie;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.annotation.JsonView;


@Entity
public class Serie {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@JsonView({com.example.demo.service.api.Views.DescripcionUsuarios.class, 
		com.example.demo.service.api.Views.DescripcionSeries.class})
	long id;
	
	@JsonView({com.example.demo.service.api.Views.DescripcionUsuarios.class, 
		com.example.demo.service.api.Views.DescripcionSeries.class})
	String nombre;
	
	@JsonView({
		com.example.demo.service.api.Views.DescripcionSeries.class})
	String sinopsis;
	
	@JsonView({com.example.demo.service.api.Views.DescripcionUsuarios.class, 
		com.example.demo.service.api.Views.DescripcionSeries.class})
	@Enumerated(EnumType.STRING)
	CategoriaSerie categoria;
	
	@JsonView({com.example.demo.service.api.Views.DescripcionSeries.class})
	@ElementCollection
	@JsonUnwrapped
	List<ActorSerie> actores = new ArrayList<ActorSerie>();
	
	public List<ActorSerie> getActores() {
		return actores;
	}

	public void setActores(List<ActorSerie> actores) {
		this.actores = actores;
	}

	@OneToMany(mappedBy = "s", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonView({com.example.demo.service.api.Views.DescripcionUsuarios.class,
		com.example.demo.service.api.Views.DescripcionSeries.class})
	@JsonUnwrapped
	Set<Temporada> temporadas;
	
	
	public Serie() {
		temporadas = new HashSet<Temporada>();
		this.categoria = CategoriaSerie.STANDART;
	}

	public int getNumTemporadas() {
		return this.temporadas.size();
	}

	public void setTemporadas(Set<Temporada> temporadas) {
		this.temporadas = temporadas;
	}

	public String getSinopsis() {
		return sinopsis;
	}

	public void setSinopsis(String sinopsis) {
		this.sinopsis = sinopsis;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public CategoriaSerie getCategoria() {
		return categoria;
	}

	/*public void setCategoria(String categoria) {
		this.categoria = CategoriaSerie.valueOf(categoria);
	}*/
	
	public void setCategoria(CategoriaSerie categoria) {
		this.categoria = categoria;
	}

	public List<Temporada> getTemporadas() {
		return new ArrayList<Temporada>(temporadas);
	}

	public boolean anhadeTemporada(String nombre) {
		Temporada t = new Temporada(this);
		t.setNombre(nombre);
		return this.temporadas.add(t);
	}
	
	public boolean anhadeActor(ActorSerie actor) {
		return this.actores.add(actor);
	}
	
	public Optional<Temporada> findTemporadaByNumber(int t) {
		return this.temporadas.stream().filter(i -> i.numero == t).findFirst();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		Serie s = (Serie) obj;
		return this.id == s.id;
	}
	
	 @Override
	 public int hashCode() {
		return Long.hashCode(this.id);
	 }
	

}
