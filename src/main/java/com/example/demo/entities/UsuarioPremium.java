package com.example.demo.entities;

import javax.persistence.Entity;


@Entity
public class UsuarioPremium extends Usuario {
	
	public static final double TARIFA_MENSUAL = 20.0;
	
	public UsuarioPremium() {
		super();
	}

	@Override
	public Factura comprobarFacturaActual() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void visualizaCapitulo(Capitulo c) {
		super.visualizaCapitulo(c);
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cobrarFacturaActual() {
		super.getFacturaMensual().total = this.TARIFA_MENSUAL;
		super.cobrarFacturaActual();
		
	}

	@Override
	public double getTotalFactura() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void facturaCapitulo(Capitulo c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getPlan() {
		// TODO Auto-generated method stub
		return "Premium";
	}


}
