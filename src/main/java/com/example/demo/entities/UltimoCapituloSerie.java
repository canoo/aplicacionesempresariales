package com.example.demo.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;


@Entity
public class UltimoCapituloSerie implements Serializable{
	
	/**
	 * 
	 */
	@Id
	@JsonIgnore
	@GeneratedValue(strategy=GenerationType.AUTO)
	long id; 
	
	private static final long serialVersionUID = -4421969834703191440L;
	@JsonView({com.example.demo.service.api.Views.DescripcionUsuarios.class})
	int capitulo;
	@JsonView({com.example.demo.service.api.Views.DescripcionUsuarios.class})
	int temporada;

	@ManyToOne
	@JsonView({com.example.demo.service.api.Views.DescripcionUsuarios.class})
	Serie s;
	
	public UltimoCapituloSerie() {
	}
	
	public UltimoCapituloSerie(Serie s) {
		this.s = s;
	}
	
	public int getCapitulo() {
		return capitulo;
	}



	public int getTemporada() {
		return temporada;
	}

	public boolean setTemporadaCapitulo(int temporada, int capitulo) {
		
		this.capitulo = capitulo;
		this.temporada = temporada;
		
		if(this.s.temporadas.size() == temporada  && this.s.findTemporadaByNumber(temporada).get().getCapitulos().size() == capitulo ) {
			return true; //termina la serie
		}
		return false;
	}

	public Serie getS() {
		return s;
	}

	public void setS(Serie s) {
		this.s = s;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
