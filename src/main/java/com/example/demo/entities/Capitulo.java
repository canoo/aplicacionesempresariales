package com.example.demo.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
public class Capitulo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8153938474111758771L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@JsonIgnore
	long id;


	@ManyToOne
	@JsonIgnore
	Temporada t;
	
	@ManyToOne
	@JsonIgnore
	Serie s;
	
	@JsonView({
		com.example.demo.service.api.Views.DescripcionSeries.class})
	int numero;
	@JsonView({
		com.example.demo.service.api.Views.DescripcionSeries.class})
	String titulo;
	@JsonView({
		com.example.demo.service.api.Views.DescripcionSeries.class})
	String enlace;
	@JsonView({
		com.example.demo.service.api.Views.DescripcionSeries.class})
	String descripcion;
	
	
	public Capitulo() {
		
	}
	public Capitulo(Temporada t) {
		this.t = t;
		this.s = t.s;
		this.numero = t.getNumCapitulos()+1;
	}
	
	public void setId(int id) {
		this.numero = id;
	}

	public Serie getS() {
		return this.t.s;
	}

	public Temporada getT() {
		return t;
	}


	public void setT(Temporada t) {
		this.t = t;
	}


	public String getTitulo() {
		return titulo;
	}


	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}


	public String getEnlace() {
		return enlace;
	}


	public void setEnlace(String enlace) {
		this.enlace = enlace;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public int getId() {
		return numero;
	}
	
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public void setId(long id) {
		this.id = id;
	}
	public void setS(Serie s) {
		this.s = s;
	}

	
	
	@Override
	public boolean equals(Object obj) {
		Capitulo c = (Capitulo) obj;
		
		if(t.numero == this.numero && this.t.equals(c.t)){
			return true;
		}
		
		return false;
	}
	
	 @Override
	 public int hashCode() {
		return numero;
	 }
	
}
