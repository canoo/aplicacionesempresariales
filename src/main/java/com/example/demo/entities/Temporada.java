package com.example.demo.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.annotation.JsonView;


@Entity
public class Temporada implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7859231487749476622L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@JsonIgnore
	long id;

	public void setId(long id) {
		this.id = id;
	}

	@ManyToOne
	@JsonIgnore
	Serie s;
	
	@JsonView({
		com.example.demo.service.api.Views.DescripcionSeries.class})
	int numero;
	@JsonView({
		com.example.demo.service.api.Views.DescripcionSeries.class})
	String nombre;
	@JsonView({
		com.example.demo.service.api.Views.DescripcionSeries.class})
	String descripcion;

	
	@OneToMany(mappedBy = "t", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonView({
		com.example.demo.service.api.Views.DescripcionSeries.class})
	@JsonUnwrapped
	Set<Capitulo> capitulos;
	
	
	public Temporada() {
		
	}
	
	public Temporada(Serie s) {
		capitulos = new HashSet<Capitulo>();
		this.s = s;
		this.numero = s.getNumTemporadas()+1;
	}
	
	public boolean anahneCapitulo( String titulo, String enlace, String descripcion) {
		
		Capitulo c = new Capitulo(this);
		c.descripcion = descripcion;
		c.enlace = c.enlace;
		c.titulo = c.titulo;
		return capitulos.add(c);
		
	}
	
	public int getId() {
		return numero;
	}

	public void setId(int id) {
		this.numero = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setCapitulos(Set<Capitulo> capitulos) {
		this.capitulos = capitulos;
	}
	
	public Optional<Capitulo> findCapituloByNumber(int capitulo) {
		return this.capitulos.stream().filter(c -> c.numero == capitulo).findFirst();
	}
	
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNumCapitulos() {
		return this.capitulos.size();
	}


	public Serie getS() {
		return s;
	}

	public void setS(Serie s) {
		this.s = s;
	}

	public List<Capitulo> getCapitulos() {
		return new ArrayList<Capitulo>(this.capitulos);
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		Temporada t = (Temporada) obj;
		
		if(t.numero == this.numero && s.id == t.s.id){
			return true;
		}
		
		return false;
	}
	
	 @Override
	 public int hashCode() {
		return numero;
	 }
	
}
