package com.example.demo.entities;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;


import com.example.demo.valueObjects.VisualizacionFactura;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.annotation.JsonView;


@Entity
public class Factura {
	
	@Id()
	@GeneratedValue(strategy=GenerationType.AUTO)
	@JsonView({com.example.demo.service.api.Views.DescripcionUsuarios.class})
	Long id;
	
	@JsonView({com.example.demo.service.api.Views.DescripcionUsuarios.class})
	Date fecha;
	
	@ManyToOne
	@JsonIgnore
	Usuario u;
	
	@JsonView({com.example.demo.service.api.Views.DescripcionUsuarios.class})
	Double total;
	
	@ElementCollection
	@JsonView({com.example.demo.service.api.Views.DescripcionUsuarios.class})
	@JsonUnwrapped
	Set<VisualizacionFactura> visualizaciones;
	
	public Factura() {
		this.visualizaciones = new HashSet<>();
	}
	
	@JsonView({com.example.demo.service.api.Views.DescripcionUsuarios.class})
	public String mesyAnho() {
		Calendar cal = Calendar.getInstance();
        cal.setTime(this.fecha);
		return (cal.get(Calendar.MONTH)+1) + " - "+ cal.get(Calendar.YEAR) ; 
	}
	
	public Factura(Usuario usu) {
		visualizaciones = new HashSet<VisualizacionFactura>();
		this.u = usu;
	}
	
	
	public void facturar(VisualizacionFactura v) {
		this.total += v.getPrecio();
		this.visualizaciones.add(v);
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Usuario getU() {
		return u;
	}

	public void setU(Usuario u) {
		this.u = u;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Set<VisualizacionFactura> getVisualizaciones() {
		return visualizaciones;
	}

	public void setVisualizaciones(Set<VisualizacionFactura> visualizaciones) {
		this.visualizaciones = visualizaciones;
	}
	

}
