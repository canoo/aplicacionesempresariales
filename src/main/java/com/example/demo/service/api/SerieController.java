package com.example.demo.service.api;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Serie;
import com.example.demo.repositorios.SerieRepositorio;
import com.fasterxml.jackson.annotation.JsonView;

@RestController
@RequestMapping("/series")

public class SerieController {
	
	@Autowired
	SerieRepositorio sr;
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping
	@JsonView(Views.DescripcionSeries.class)
	List<Serie> todosSeries() {
		return sr.findAll();
	}
	

	@GetMapping("/{idSerie}")
	@JsonView(Views.DescripcionUsuarios.class)
	public ResponseEntity<Serie> getViaje(@PathVariable Long idSerie) {
			
			ResponseEntity<Serie> result;
			
			Optional<Serie> v = sr.findById(idSerie);
			
			if (v.isPresent()) {
				result = ResponseEntity.ok(v.get());
			} else {
				result = ResponseEntity.notFound().build();
			}
			
			return result;
		}
	
	
}
