package com.example.demo.service.api;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Factura;
import com.example.demo.entities.Usuario;
import com.example.demo.repositorios.UsuarioRepositorio;
import com.example.demo.service.ResourceNotFound;
import com.example.demo.service.UsuarioMng;

import com.fasterxml.jackson.annotation.JsonView;



@RestController
@RequestMapping("/usuarios")

public class UsuarioController {
	
	@Autowired
	UsuarioRepositorio ur;
	
	@Autowired
	UsuarioMng umng;
	
    
	@GetMapping
	@JsonView(Views.DescripcionUsuarios.class)
	List<Usuario> todosUsuarios() {
		return ur.findAll();
	}
	
	@GetMapping("/{username}")
	@CrossOrigin(origins = "http://localhost:4200")
	@JsonView(Views.DescripcionUsuarios.class)
	public ResponseEntity<Usuario> getFacturasUsuario(@PathVariable String username) {
		
		ResponseEntity<Usuario> result;
		
		Optional<Usuario> v = ur.findById(username);
		
		if (v.isPresent()) {
			result = ResponseEntity.ok(v.get());
		} else {
			result = ResponseEntity.notFound().build();
		}
		
		return result;
	}
	
	@GetMapping("/{username}/facturas")
	@JsonView(Views.DescripcionUsuarios.class)
	List<Factura> getUsuario(@PathVariable String username) {
		
		List<Factura> result = null;
		
		Optional<Usuario> v = ur.findById(username);
		
		if (v.isPresent()) {
			result =  v.get().getHistoricoFacturas();
		}
		
		return result;
	}
	
	
	@GetMapping("/{username}/facturas/{idFactura}")
	@JsonView(Views.DescripcionUsuarios.class)
	public ResponseEntity<Factura> getFactura(@PathVariable String username,@PathVariable Long idFactura) {
		
		Optional<Usuario> v = ur.findById(username);
		Optional<Factura> f = null;
		ResponseEntity<Factura> result = null;
		
		if (v.isPresent()) {
			
			f =  v.get().getFacturaById(idFactura);
			if(f.isPresent()) {
				result = ResponseEntity.ok(f.get());
			}
		}
		
		return result;
	}
	

	
	@PostMapping("/{username}/series/{serieId}/anhadir")
	@CrossOrigin(origins = "http://localhost:4200")
	@JsonView(Views.DescripcionUsuarios.class)
	public ResponseEntity<Usuario> anhadirPendiente(@PathVariable String username, @PathVariable long serieId) {
		
		ResponseEntity<Usuario> result;
		try {
			Usuario f = umng.anhadeCapituloAPendiente(username, serieId);
		if (f != null) {
			result = ResponseEntity.ok(f);
		} else {
			result = new ResponseEntity<Usuario>(HttpStatus.FORBIDDEN);
		}
		
		} catch (ResourceNotFound e) {
			result = ResponseEntity.notFound().build();
		}
		
		return result;
	}

	
	@PostMapping("/{username}/series/{serieId}/visualizar")
	@CrossOrigin(origins = "http://localhost:4200")
	@JsonView(Views.DescripcionUsuarios.class)
	public ResponseEntity<Usuario> visualizaCapitulo(@PathVariable String username, @PathVariable long serieId, @RequestParam(required=true) int temporadaNum,
			@RequestParam(required=true) int capituloNum) {
		
		ResponseEntity<Usuario> result = null;
		
		try {
			Usuario f = umng.visualizaCapitulo(username, serieId, temporadaNum, capituloNum);
		if (f != null) {
			result = ResponseEntity.ok(f);
		} else {
			result = new ResponseEntity<Usuario>(HttpStatus.FORBIDDEN);
		}
		} catch (ResourceNotFound e) {
			result = ResponseEntity.notFound().build();
		}
		
		return result;
	}
	
	
	

	@PostMapping
	public ResponseEntity<Usuario> registroUsuario(@RequestBody 
			@JsonView(Views.DescripcionUsuarios.class) Usuario u) {
		ResponseEntity<Usuario> result;
		
		if (ur.existsById(u.getNombre())) {
			result = ResponseEntity.status(HttpStatus.FORBIDDEN).build();
		} else {
			u = ur.save(u);
			URI resourceLink = null;
			try {
				resourceLink = new URI("/usuarios/" + u.getNombre());
			} catch (URISyntaxException e) {
			}
			result = ResponseEntity.created(resourceLink).build();
		}
		
		return result;
		
	}
	
	
}
