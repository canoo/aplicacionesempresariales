package com.example.demo.service;


import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entities.Capitulo;
import com.example.demo.entities.Serie;
import com.example.demo.entities.Temporada;
import com.example.demo.entities.Usuario;
import com.example.demo.repositorios.SerieRepositorio;
import com.example.demo.repositorios.UsuarioRepositorio;

@Service
public class UsuarioMng {
	
	@PersistenceUnit
	EntityManagerFactory emf;
	
	@Autowired
	SerieRepositorio rs;
	@Autowired
	UsuarioRepositorio ur;
	//@Autowired
	//FacturaRepositorio fr;
	
	
	@Transactional
	public Usuario visualizaCapitulo(String username,long serieId, int temporadaId, int capituloId) throws ResourceNotFound {
		
		Usuario user = ur.findById(username).orElseThrow(ResourceNotFound::new);
		Serie s = rs.findById(serieId).orElseThrow(ResourceNotFound::new); //accedidas al repositorio
		
		Temporada t = s.findTemporadaByNumber(temporadaId).orElseThrow(ResourceNotFound::new); //accedido desde el agregateroot
		Capitulo c = t.findCapituloByNumber(capituloId).orElseThrow(ResourceNotFound::new);
		
		user.visualizaCapitulo(c);
		
		return user;
	}
	
	@Transactional
	public Usuario anhadeCapituloAPendiente(String username, long idSerie) throws ResourceNotFound {
		
		Usuario user = ur.findById(username).orElseThrow(ResourceNotFound::new);
		Serie s = rs.findById(idSerie).orElseThrow(ResourceNotFound::new); //accedidas al repositorio
		user.anhadeSerieAPendientes(s);
		return user;
	}

}
