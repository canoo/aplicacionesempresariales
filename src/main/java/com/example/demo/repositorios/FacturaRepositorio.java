package com.example.demo.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entities.Factura;

/*No se usa*/
public interface FacturaRepositorio extends JpaRepository<Factura, Long> {
	
	/*@Query("SELECT f FROM Factura v WHERE f.date >= ?1  AND v.date <= ?2")
	public List<Factura> findByDate(String fecha1, String fecha2);*/
	
}