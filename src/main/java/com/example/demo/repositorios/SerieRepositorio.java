package com.example.demo.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entities.Serie;



public interface SerieRepositorio extends JpaRepository<Serie, Long> {}