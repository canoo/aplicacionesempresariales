package com.example.demo.valueObjects;

import java.util.Date;

import javax.persistence.Embeddable;

import com.fasterxml.jackson.annotation.JsonView;

@Embeddable
public class VisualizacionFactura {
	

	private long serieId;
	@JsonView({com.example.demo.service.api.Views.DescripcionUsuarios.class})
	String serie;
	@JsonView({com.example.demo.service.api.Views.DescripcionUsuarios.class})
	int numTemporada;
	@JsonView({com.example.demo.service.api.Views.DescripcionUsuarios.class})
	int numeroCapitulo;
	@JsonView({com.example.demo.service.api.Views.DescripcionUsuarios.class})
	Date fecha;
	@JsonView({com.example.demo.service.api.Views.DescripcionUsuarios.class})
	Double precio;
	
	
	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public int getNumTemporada() {
		return numTemporada;
	}

	public void setNumTemporada(int numTemporada) {
		this.numTemporada = numTemporada;
	}

	public int getNumeroCapitulo() {
		return numeroCapitulo;
	}

	public void setNumeroCapitulo(int numeroCapitulo) {
		this.numeroCapitulo = numeroCapitulo;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	@Override
	public boolean equals(Object obj) {
		VisualizacionFactura v = (VisualizacionFactura) obj;
		return v.getSerieId() == this.getSerieId();
	}
	
	 @Override
	 public int hashCode() {
		return Long.hashCode(this.getSerieId());
	 }

	public long getSerieId() {
		return serieId;
	}

	public void setSerieId(long idSerie) {
		this.serieId = idSerie;
	}
}
