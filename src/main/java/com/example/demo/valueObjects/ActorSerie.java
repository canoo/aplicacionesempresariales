package com.example.demo.valueObjects;

import javax.persistence.Embeddable;

import com.fasterxml.jackson.annotation.JsonView;

@Embeddable
public class ActorSerie {
	@JsonView({com.example.demo.service.api.Views.DescripcionSeries.class})
	public String nombre;

}
