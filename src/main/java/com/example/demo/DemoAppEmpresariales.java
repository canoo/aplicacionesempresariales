	package com.example.demo;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.example.demo.entities.Capitulo;
import com.example.demo.entities.Factura;
import com.example.demo.entities.Serie;
import com.example.demo.entities.Temporada;
import com.example.demo.entities.Usuario;
import com.example.demo.entities.UsuarioNormal;
import com.example.demo.entities.UsuarioPremium;
import com.example.demo.repositorios.SerieRepositorio;
import com.example.demo.repositorios.UsuarioRepositorio;
import com.example.demo.valueObjects.ActorSerie;
import com.example.demo.valueObjects.CategoriaSerie;
import com.example.demo.valueObjects.VisualizacionFactura;


@SuppressWarnings("unused")
@SpringBootApplication
//@EnableJpaRepositories("repositorios")
//@EntityScan("entities")
//@ComponentScan("valueObjects")
public class DemoAppEmpresariales {

	public static void main(String[] args) {

		SpringApplication.run(DemoAppEmpresariales.class, args);
	}
	
	@Bean
	public CommandLineRunner demo(UsuarioRepositorio ur, SerieRepositorio sr) { //
	    return (args) -> {
	    	
			
			Usuario u = new UsuarioNormal();
			u.setNombre("Alex");
			u.setIBAN("dfa");
			u.setPassword("4444");
			
			
			Usuario u2 = new UsuarioPremium();
			u2.setNombre("Ivan");
			u2.setIBAN("sdagas");
			u2.setPassword("4444");
	    	
			Serie s = new Serie();
			s.setNombre("Friends");
			s.setCategoria(CategoriaSerie.GOLD);
			s.setSinopsis("Vestibulum non vehicula purus, ornare tempor odio."
					+ " Vivamus viverra, tortor sed efficitur facilisis, quam felis mattis sem,"
					+ "id sit amet ligula.");

			s.anhadeTemporada("T1");
			s.anhadeTemporada("T2");
			s.anhadeTemporada("T3");
			
			ActorSerie a = new ActorSerie();
			a.nombre = "Matt LeBlanc";
			
			ActorSerie a2 = new ActorSerie();
			a2.nombre = "Jennifer Aniston";
			
			s.anhadeActor(a);
			s.anhadeActor(a2);
			
			s.getTemporadas().forEach(t ->{
				t.anahneCapitulo(t.getNombre() + ": C1","http://...","descripcion");
				t.anahneCapitulo(t.getNombre() + ": C2","http://...","descripcion");
				t.anahneCapitulo(t.getNombre() + ": C3","http://...","descripcion");
				t.anahneCapitulo(t.getNombre() + ": C4","http://...","descripcion");
				t.anahneCapitulo(t.getNombre() + ": C5","http://...","descripcion");
				t.anahneCapitulo(t.getNombre() + ": C6","http://...","descripcion");
			});
			
			
			Serie s2 = new Serie();
			ActorSerie b1 = new ActorSerie();
			b1.nombre = "Rami Malek";
			s2.anhadeActor(b1);
			s2.setNombre("Mr robot");
			s2.setCategoria(CategoriaSerie.STANDART);
			s2.setSinopsis("Quisque mollis tincidunt efficitur. Integer neque massa, ultrices vel sapien faucibus, "
					+ "varius ipsum egestas vitae. Etiam venenatis nisl eu lorem venenatis laoreet. Nulla bibendum odio "
					+ "Mauris ac vestibulum sem.");
			s2.anhadeTemporada("T1");
			s2.anhadeTemporada("T2");
			
			s2.getTemporadas().forEach(t ->{
				t.anahneCapitulo(t.getNombre() + ": C1","http://...","descripcion c1");
				t.anahneCapitulo(t.getNombre() + ": C2","http://...","descripcion c2");
				t.anahneCapitulo(t.getNombre() + ": C3","http://...","descripcion c3");

			});
			
			Serie s3 = new Serie();
			ActorSerie c1 = new ActorSerie();
			c1.nombre = "Christine Baranski";
			s3.anhadeActor(b1);
			s3.setNombre("The good fight");
			s3.setCategoria(CategoriaSerie.STANDART);
			s3.setSinopsis("Quisque mollis tincidunt efficitur. Integer neque massa, ultrices vel sapien faucibus, "
					+ "Mauris ac vestibulum sem.");
			s3.anhadeTemporada("T1");
			s3.anhadeTemporada("T2");
			s3.getTemporadas().forEach(t ->{
				t.anahneCapitulo(t.getNombre() + ": C1","http://...","descripcion");
			});
			
			sr.save(s);
			sr.save(s2);
			sr.save(s3);
			
			u.anhadeSerieAPendientes(s);
			//u.anhadeSerieAPendientes(s2);
			u.anhadeSerieAPendientes(s3);
			
			u.visualizaCapitulo(s.getTemporadas().get(0).getCapitulos().get(0));
			u.visualizaCapitulo(s3.getTemporadas().get(0).getCapitulos().get(0));
			
			u.cobrarFacturaActual();
			
			u.visualizaCapitulo(s.getTemporadas().get(1).getCapitulos().get(2));
			u.visualizaCapitulo(s3.getTemporadas().get(1).getCapitulos().get(0));
			
			u.cobrarFacturaActual();
			
			u.visualizaCapitulo(s.getTemporadas().get(2).getCapitulos().get(1));
			u.visualizaCapitulo(s3.getTemporadas().get(1).getCapitulos().get(0));
			u.cobrarFacturaActual();
			/*List<Factura> fs = new ArrayList<Factura>();
			
			fs.add(new Factura(u));
			fs.add(new Factura(u));
			fs.add(new Factura(u));
			
			
			fs.forEach(f ->{
				
				int i = fs.indexOf(f);
				
				f.setTotal(0.0);
				f.setFecha(new Date());
				
				VisualizacionFactura v = new VisualizacionFactura();
				v.setSerie(s.getNombre());
				v.setSerieId(s.getId());
				v.setNumTemporada(1);
				v.setNumeroCapitulo(i);
				v.setPrecio(1.5);
				v.setFecha(new Date());
				
				VisualizacionFactura v2 = new VisualizacionFactura();
				v2.setSerie(s2.getNombre());
				v2.setSerieId(s2.getId());
				v2.setNumTemporada(1);
				v2.setNumeroCapitulo(i);
				v2.setPrecio(1.5);
				v2.setFecha(new Date());
				
				f.facturar(v);
				f.facturar(v2);
			});
			
			fs.add(u.getFacturaMensual());
			u.setHistoricoFacturas(fs);*/
			
			ur.save(u);
			ur.save(u2);
			

			
			
	    };
	  }

}
